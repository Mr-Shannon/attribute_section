#include "stm32f1xx_hal.h"
#include "board.h"

/**
  * @brief This function __attribute__((section())) start section.
  * @note These automatically initializaiton.
  * @param None.
  * @retval 0.
  */    
static int _start(void)
{
    return 0;
}
INIT_FUN(_start, "0");
/**
  * @brief This function __attribute__((section())) end section. 
  * @note These automatically initializaiton.
  * @param None.
  * @retval 0.
  */
static int _end(void)
{
    return 0;
}
INIT_FUN(_end, "7");

/**
  * @brief This function initializes the Global MSP.
  * @note Initializes the Global MSP.
  * @param None.
  * @retval None.
  */
void HAL_MspInit(void)
{
    HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
    /* System interrupt init*/
    __HAL_RCC_AFIO_CLK_ENABLE();
    /* MemoryManagement_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);
    /* BusFault_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(BusFault_IRQn, 0, 0);
    /* UsageFault_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(UsageFault_IRQn, 0, 0);
    /* SVCall_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(SVCall_IRQn, 0, 0);
    /* DebugMonitor_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DebugMonitor_IRQn, 0, 0);
    /* PendSV_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(PendSV_IRQn, 15, 0);
    /* SysTick_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
    /**DISABLE: JTAG-DP Disabled and SW-DP Disabled**/
    __HAL_AFIO_REMAP_SWJ_NOJTAG();
}
/**
  * @brief This function configures the source of the time base.
  * @note Configures the source of the time base.
  * @param TickPriority Tick interrupt priority.
  * @retval HAL status
  */
void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    /**Initializes the CPU, AHB and APB busses clocks
      */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
    if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        while(1);
    }
    /**Initializes the CPU, AHB and APB busses clocks
      */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | 
                                  RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
    if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
    {
        while(1);
    }
    /**Configure the Systick interrupt time
      */
    HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);
    /**Configure the Systick
      */
    HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
    /* SysTick_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}
/**
  * @brief This is the timer interrupt service routine.
  * @note The timer interrupt service routine.
  * @param None.
  * @retval None.
  */
void SysTick_Handler(void)
{
    HAL_IncTick();
}
/**
  * @brief This function will initial board.
  * @note Initial board.
  * @param None.
  * @retval None.
  */
void board_init(void)
{
    const init_fn_t *fn_ptr;
    
    HAL_Init();
    SystemClock_Config();

    for (fn_ptr = &__init_start; fn_ptr < &__init_end; fn_ptr++)
    {
        (*fn_ptr)();
    }
}
/**
  * @brief This function Re-define main function.
  * @note Re-define main function. 
  * @param None.
  * @retval None.
  */
#if defined (__CC_ARM)
extern int $Super$$main(void);
/* re-define main function */
int $Sub$$main(void)
{
    board_init();
    $Super$$main();
    return 0;
}
#elif defined(__ICCARM__)
extern int main(void);
/* __low_level_init will auto called by IAR cstartup */
extern void __iar_data_init3(void);
int __low_level_init(void)
{
    // call IAR table copy function.
    __iar_data_init3();
    board_init();
    return 0;
}
#elif defined(__GNUC__)
extern int main(void);
/* Add -eentry to arm-none-eabi-gcc argument */
int entry(void)
{
    board_init();
    return 0;
}
#endif /* defined (__CC_ARM) */
