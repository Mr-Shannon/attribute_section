#include "drv_usart.h"
#include "stm32f1xx_hal.h"
#include "board.h"

UART_HandleTypeDef huart;

static int stm32_uart_configure(void)
{
    HAL_UART_DeInit(&huart);
    huart.Instance       		= USART1;
    huart.Init.BaudRate   		= 115200;
    huart.Init.WordLength 		= UART_WORDLENGTH_8B;
    huart.Init.StopBits   		= UART_STOPBITS_1;
    huart.Init.Parity     		= UART_PARITY_NONE;
    huart.Init.HwFlowCtl  		= UART_HWCONTROL_NONE;
    huart.Init.Mode         	= UART_MODE_TX_RX;
    if (HAL_UART_Init(&huart) != HAL_OK)
    {
        while(1);
    }
    return 0;
}
INIT_DRIVER(stm32_uart_configure);

int stm32_putc(char c)
{
    while (__HAL_UART_GET_FLAG(&huart, UART_FLAG_TXE) == RESET);
    huart.Instance->DR = c;
    return 0;
}

int stm32_getc(void)
{
    int ch;
    
    ch = -1;
    if (__HAL_UART_GET_FLAG(&huart, UART_FLAG_RXNE) != RESET)
    {
        ch = huart.Instance->DR & 0xff;
    }
    return ch;
}

/* USART2 init function */

void HAL_UART_MspInit(UART_HandleTypeDef *uartHandle)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    
    if (uartHandle->Instance == USART1)
    {
        /* USART1 clock enable */
        __HAL_RCC_USART1_CLK_ENABLE();
        __HAL_RCC_GPIOA_CLK_ENABLE();
        /**USART1 GPIO Configuration
        PA9     ------> USART1_TX
        PA10     ------> USART1_RX
        */
        GPIO_InitStruct.Pin = GPIO_PIN_9;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
        GPIO_InitStruct.Pin = GPIO_PIN_10;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
        /* USART1 interrupt Init */
        HAL_NVIC_SetPriority(USART1_IRQn, 5, 0);
        HAL_NVIC_EnableIRQ(USART1_IRQn);
    }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef *uartHandle)
{
    if (uartHandle->Instance == USART1)
    {
        /* Peripheral clock disable */
        __HAL_RCC_USART1_CLK_DISABLE();
        /**USART1 GPIO Configuration
        PA9     ------> USART1_TX
        PA10     ------> USART1_RX
        */
        HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9 | GPIO_PIN_10);
        /* USART1 interrupt Deinit */
        HAL_NVIC_DisableIRQ(USART1_IRQn);
    }
}

int fputc(int ch, FILE *f)   //Printf
{
    stm32_putc((uint8_t)ch);
    return ch;
}
