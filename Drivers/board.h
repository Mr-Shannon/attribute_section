#ifndef __BOARD_H__
#define __BOARD_H__

/* Compiler Related Definitions */
#if defined (__CC_ARM) || defined (__GNUC__)    /* GNU GCC & ARM Compiler */
    #define SECTION(x)                  __attribute__((section(x)))
#elif defined (__IAR_SYSTEMS_ICC__)             /* for IAR Compiler */
    #define SECTION(x)                  @ x
#endif /* Compiler Related Definitions */

typedef int (*init_fn_t)(void);
#define INIT_FUN(fn, level)  \
            const init_fn_t __init##fn SECTION(".init_fn."level) = fn
#define INIT_BOARD(fn)              INIT_FUN(fn, "1")
#define INIT_DRIVER(fn)             INIT_FUN(fn, "2")
#define INIT_DEVICE(fn)             INIT_FUN(fn, "3")
#define INIT_APP(fn)                INIT_FUN(fn, "4")

#endif /* __BOARD_H__ */
